import Component from 'can-component';
import view from './todo.stache'

Component.extend({
    view: view,
    tag: "todo-component",
    viewModel: {
        destroy: function () {
            this.dispatch("destroy");
        },
        edit: function () {
            this.dispatch("edit");
        }
    }
})