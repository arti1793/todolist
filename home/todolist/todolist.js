import Component from 'can-component';
import view from './todolist.stache'
import Map from 'can-map';
import List from 'can-list';
import 'can-map-define';
import "can-list-sort";

let TodoList = List.extend({
    init: function () {
        this.findAll();
    },
    comparator: "title",
    findAll: function (params) {
        this.splice(0, this.length);
        return this.push.apply(this, Object.keys(localStorage)
            .filter(val => /todo\/(\d+)/.test(val))
            .map(key => {
                let todo = new Todo(JSON.parse(localStorage[key]))
                todo.attr("id", +key.split("/")[1]);
                return todo
            }))
    },
    findOne: function (params) {
        return new Todo(localStorage["todo/" + params.id]);
    },
    destroy: function (index) {
        this[index].destroy();
        return this.splice(index, 1);
    },
})

let Todo = Map.extend({
    lastkey: function () {
        let ids = Object.keys(localStorage)
            .filter(val => /todo\/(\d+)/.test(val))
            .map(key => key.split("/")[1]);
        return Math.max.apply(null, ids.length ? ids : [0])
    },
    create: function () {
        let id = this.lastkey() + 1;
        this.attr("id", id);
        localStorage["todo/" + id] = JSON.stringify(this.serialize());
    },
    save: function () {
        if (this.id) {
            localStorage["todo/" + this.id] = JSON.stringify(this.serialize());
        } else {
            this.create();
        }
    },
    destroy: function () {
        delete localStorage["todo/" + this.id];
    },
});

let ViewModel = Map.extend({
    todos: new TodoList,
    newTodo: new Todo,
    add: function () {
        if (this.newTodo.title) {
            this.newTodo.save();
            this.todos.push(this.newTodo);
            this.attr("newTodo", new Todo);
        } else {
        
        }
    },
    edit: function (index) {
        this.attr("newTodo", this.todos[index])
        this.todos.splice(index, 1);
    },
    sort: function (field, asc) {
        this.todos.attr("comparator", function (a, b) {
            let afield = a.attr(field);
            let bfield = b.attr(field);

            if (asc === 1) {
                return afield <= bfield ? +1 : -1
            } else {
                return afield > bfield ? +1 : -1
            }
        })
    }
});

export default Component.extend({
    tag: "todolist-component",
    view,
    viewModel:new ViewModel
})